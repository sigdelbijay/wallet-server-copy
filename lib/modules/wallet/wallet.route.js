const walletRouter = (() => {
    'use strict';

    const express = require('express');
    const walletRouter = express.Router();
    const walletController = require('./wallet.controller');

    walletRouter.route('/create-apikey')
        .post(walletController.createApiKey);

    walletRouter.route('/create-seed/:apiKey')
        .put(walletController.createSeed);

    walletRouter.route('/create-wallet/:apiKey')
        .post(walletController.createWallet);

    walletRouter.route('/create-transaction/:apiKey')
        .post(walletController.createTransaction);

    return walletRouter;

})();

module.exports = walletRouter;
