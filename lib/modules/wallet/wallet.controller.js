const walletController = (() => {
    'use strict';

    const HTTPStatus = require('http-status');
    const bip39 = require('bip39');
    const dashcore = require("bitcore-lib-dash");
    const request = require("request-promise");
    const {ObjectId} = require('mongodb');
    
    const moduleConfig = require('./wallet.config');
    const errorHelper = require('../../helpers/error.helper');
    const commonHelper = require('../../common/common-helper-function');
    // const utilityHelper = require('../../helpers/utilities.helper');
    // const commonProvider = require('../../common/common-provider-function');
    // const redisHelper = require('../../helpers/redis.helper');


    // const documentFields = 'exchangeName cyptocurrency domainName';
    // const projectionFields = {
    //     '_id': 1,
    //     'exchangeName': 1,
    //     'cyptocurrency': 1,
    //     'domainName': 1,
    //     'added_on': 1
    // };


    function walletModule() {}
    const _p = walletModule.prototype;

    _p.checkCreateApiKeyValidationErrors = async(req) => {
        req.checkBody('exchangeName', moduleConfig.message.validationErrMessage.exchangeName).notEmpty();
        req.checkBody('cyptocurrency', moduleConfig.message.validationErrMessage.cyptocurrency).notEmpty();
        req.checkBody('cyptocurrencyIndex', moduleConfig.message.validationErrMessage.cyptocurrencyIndex).notEmpty();      
        req.checkBody('domainName', moduleConfig.message.validationErrMessage.domainName).notEmpty();
        const result = await req.getValidationResult();
        return result.array();
    };

    _p.randomKeyGenerator = async (req) => {
        let randomKey, result;
        do {
            randomKey = commonHelper.generateRandomBytes(16);
            result = await req.db.collection('exchange_account').findOne({'api_arr.api_key': randomKey});
        } while (result);

        return randomKey;
    };

    _p.mapApiArr = (data, objToMap) => {
        const keys = Object.keys(objToMap);
        let obj;
        data.api_arr.forEach(item => {
            if(item[keys[0]] === objToMap[keys[0]]) {
                obj =  {
                    _id: data._id,
                    exchange_name : data.exchange_name,
                    domain_name: data.domain_name,
                    api_arr: [item]
                }
            }
        })
        return obj;
    } 

    _p.createApiKey = async(req, res, next) => {
        try {

            const errors = await _p.checkCreateApiKeyValidationErrors(req);
            if(errors && errors.length){
                return commonHelper.sendResponseData(res, {
                    status: HTTPStatus.BAD_REQUEST,
                    message: errorHelper.sendFormattedErrorData(errors)
                });
            }
            
            const api_key = await _p.randomKeyGenerator(req);
            const domain_name = req.body.domainName;
            const cryptocurrency = req.body.cryptocurrency;
            const exchangeAccount = await req.db.collection('exchange_account').findOne({domain_name});
            let dataRes, existingData, exchangeInfo;

            if(exchangeAccount) {
                existingData = await _p.mapApiArr(exchangeAccount, {cryptocurrency});
                if(!existingData) {
                    const apiArrElem = {
                        api_key,
                        cryptocurrency: req.body.cyptocurrency,
                        cryptocurrency_index: req.body.cyptocurrencyIndex
                    };
                    
                    const queryOpts = {
                        _id: exchangeAccount._id
                    };
            
                    const updateOpts = {
                        $push: {
                            api_arr: apiArrElem
                        }
                    };
                    
                    exchangeInfo = {
                        _id: exchangeAccount._id,
                        exchange_name : exchangeAccount.exchange_name,
                        domain_name: exchangeAccount.domain_name,
                        api_arr: [apiArrElem]
                    };
                    dataRes = await req.db.collection('exchange_account').update(queryOpts, updateOpts);
                }
            } else {
                exchangeInfo = {
                    _id: ObjectId(),
                    exchange_name : req.body.exchangeName,
                    domain_name,
                    api_arr: [
                        {
                            api_key,
                            cryptocurrency: req.body.cyptocurrency,
                            cryptocurrency_index: req.body.cyptocurrencyIndex
                        }
                    ]
                };
                dataRes = await req.db.collection('exchange_account').save(exchangeInfo);
            }
            
            res.status(200).json({
                data: exchangeInfo || existingData,
                message: exchangeInfo ? moduleConfig.message.create : (existingData ? moduleConfig.message.alreadyExists : moduleConfig.message.create)
            })
            // return commonHelper.sendJsonResponseMessage(res, dataRes, exchangeInfo, moduleConfig.message.create);

        } catch (err) {
            return next(err);
        }
    };

    _p.checkApiKeyValidationErrors = async(req) => {
        req.checkBody('apiKey', moduleConfig.message.validationErrMessage.apiKey).notEmpty();
        const result = await req.getValidationResult();
        return result.array();
    };

    _p.createSeed = async(req, res, next) => {
        try {

            const api_key = req.params.apiKey;
            const seed = bip39.generateMnemonic();
            console.log("seed", seed);
            const queryOpts = {
                'api_arr.api_key': api_key
            };
            const exchangeAccount = await req.db.collection('exchange_account').findOne(queryOpts);
            console.log("exchangeAccount", exchangeAccount);
            
            if (!exchangeAccount) {
                return commonHelper.sendResponseData(res, {
                    status: HTTPStatus.BAD_REQUEST,
                    message: moduleConfig.message.notFound
                });
            }

            const existingData = await _p.mapApiArr(exchangeAccount, {api_key});
            console.log("existingData", existingData);
            if(existingData.api_arr[0].seed) {
                return commonHelper.sendResponseData(res, {
                    status: HTTPStatus.CONFLICT,
                    message: moduleConfig.message.seedAlreadyExists
                });
            }
            existingData.api_arr[0].seed = seed;
            
            const updateOpts = {
                $set: {
                    'api_arr.$.seed': existingData.api_arr[0].seed
                }
            };

            const dataRes = await req.db.collection('exchange_account').updateOne(queryOpts, updateOpts);
            res.status(200).json({
                data: existingData,
                message: moduleConfig.message.createSeed
            })
            // return commonHelper.sendJsonResponseMessage(res, dataRes, existingData, moduleConfig.message.create);

        } catch (err) {
            return next(err);
        }
    };

    _p.createWallet = async(req, res, next) => {
        
        const api_key = req.params.apiKey;
        const queryOpts = {
            'api_arr.api_key': api_key
        };
        const exchangeAccount = await req.db.collection('exchange_account').findOne(queryOpts);
        if (!exchangeAccount) {
            return commonHelper.sendResponseData(res, {
                status: HTTPStatus.BAD_REQUEST,
                message: moduleConfig.message.notFound
            });
        }
        const existingData = await _p.mapApiArr(exchangeAccount, {api_key});
        const seed = existingData.api_arr[0].seed;
        const buffer = bip39.mnemonicToSeedHex(seed);
        const cIndex = existingData.api_arr[0].cryptocurrency_index;
        let address_index;
        const maxAddressIndex = await req.db.collection('wallet').aggregate([
            {
                $project: { $max: '$address_index' }
            }
        ]);
        if(maxAddressIndex) {
            address_index = maxAddressIndex + 1;
        } else {
            address_index = 0;
        }

        const hdPrivateKey =  dashcore.HDPrivateKey.fromSeed(buffer);
        const derived = hdPrivateKey.derive(`m/44'/${cIndex}'/0/0/${address_index}`);
        const address = derived.privateKey.toAddress(dashcore.Networks.testnet);
        const walletInfo = {
            _id: ObjectId(),
            api_key,
            address: address.toString(),
            xpriv: hdPrivateKey.toString(),
            privatekey: derived.privateKey.toString(),
            address_index

        };

        const dataRes = await req.db.collection('wallet').save(walletInfo);

        res.status(200).json({
            data: walletInfo,
            message: moduleConfig.message.createWallet
        })

    };

    _p.checkCreateTransactionValidationErrors = async(req) => {
        req.checkBody('from', moduleConfig.message.validationErrMessage.from).notEmpty();
        req.checkBody('to', moduleConfig.message.validationErrMessage.to).notEmpty();
        req.checkBody('amount', moduleConfig.message.validationErrMessage.amount).notEmpty();        
        const result = await req.getValidationResult();
        return result.array();
    };

    _p.createTransaction = async (req, res, next) => {

        const errors = await _p.checkCreateTransactionValidationErrors(req);
        if(errors && errors.length){
            return commonHelper.sendResponseData(res, {
                status: HTTPStatus.BAD_REQUEST,
                message: errorHelper.sendFormattedErrorData(errors)
            });
        }
        
        const from = req.body.from;
        const to = req.body.to;
        let amount = req.body.amount; 
        const api_key = req.params.apiKey;
        const queryOpts = {
            'api_arr.api_key': api_key
        };
        const exchangeAccount = await req.db.collection('exchange_account').findOne(queryOpts);
        if (!exchangeAccount) {
            return commonHelper.sendResponseData(res, {
                status: HTTPStatus.BAD_REQUEST,
                message: moduleConfig.message.notFound
            });
        }

        const existingData = await _p.mapApiArr(exchangeAccount, {api_key});
        const seed = existingData.api_arr[0].seed;
        const buffer = bip39.mnemonicToSeedHex(seed);
        const cIndex = existingData.api_arr[0].cryptocurrency_index;
        let address_index;
        const maxAddressIndex = await req.db.collection('wallet').aggregate([
            {
              $match: { api_key }
            },
            {
                $project: {
                    address_index: { $max: '$address_index' }
                }
            }
        ]).toArray();

        if(maxAddressIndex[0].address_index) {
            address_index = maxAddressIndex.address_index + 1;
        } else {
            address_index = 0;
        }

        const unit = dashcore.Unit;
        // var body = req.body;
        var hdPrivateKey = dashcore.HDPrivateKey.fromSeed(buffer);
        var derived = hdPrivateKey.derive(`m/44'/${cIndex}'/0/0/${address_index}`);
        var address = derived.privateKey.toAddress(dashcore.Networks.testnet).toString();
        var privateKey = derived.privateKey.toString();

        amount = unit.fromMilis(amount).toSatoshis();
        const minerFee = unit.fromMilis(0.128).toSatoshis(); // cost of transaction in satoshis (minerfee)

        var options = {
            method: 'POST',
            uri: 'https://test.insight.dash.siampm.com/api/addrs/utxo',
            body: {
                addrs: address
            },
            json: true
        };
        const utxos = await request(options);

        if (utxos && utxos.length) {

            // get balance
            let balance = unit.fromSatoshis(0).toSatoshis();
            for (let i = 0; i < utxos.length; i++) {
                balance += unit.fromSatoshis(parseInt(utxos[i]['satoshis'])).toSatoshis();
            }

            // check whether the balance of the address covers the miner fee
            if ((balance - amount - minerFee) > 0) {

                privateKey = new dashcore.PrivateKey(privateKey, dashcore.Networks.testnet);

                const minerFee = unit.fromMilis(0.128).toSatoshis(); // cost of transaction in satoshis (minerfee)
                const transactionAmount = unit.fromMillis(amount).toSatoshis(); // convert BTC to Satoshis using bitcore unit

                try {
                    var transaction = new dashcore.Transaction()
                        .from(utxos)
                        .to(to, transactionAmount)
                        .fee(minerFee)
                        .change(address)
                        .sign(privateKey)
                        .serialize();

                    var options = {
                        method: 'POST',
                        uri: 'https://test.insight.dash.siampm.com/api/tx/send',
                        body: {
                            rawtx: transaction
                        },
                        json: true
                    };

                    request(options)
                        .then(async(txid) => {

                            const transactionInfo = {
                                _id: ObjectId(),
                                from,
                                to,
                                amount,
                                txhash: txid
                            };
                            const dataRes = await req.db.collection('transaction').save(transactionInfo);
                            return res.status(200).json({
                                success: 1,
                                txhash: txid
                            });

                        })

                }
                catch (e) {
                    return res.status(500).json({
                        message: e.message
                    });
                }
            }
            else {
                return res.status(400).json({
                    success: 0,
                    address: address,
                    txhash: "You don't have enough Satoshis to cover the miner fee."
                });
            }
        }
        else {
            return res.status(400).json({
                success: 0,
                address: address,
                txhash: "user has no fund"
            });

        }


    }



    return {
        createApiKey: _p.createApiKey,
        createSeed: _p.createSeed,
        createWallet: _p.createWallet,
        createTransaction: _p.createTransaction
    };
})();

module.exports = walletController;
