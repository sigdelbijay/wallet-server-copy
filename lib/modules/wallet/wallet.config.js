(() => {
    "use strict";

    module.exports = {
        message: {
            create: "API key created successfully",
            createSeed: "Seed created successfully",
            createWallet: "Wallet created successfully",
            notFound: "API key not found",
            alreadyExists: "Domain already exists",
            seedAlreadyExists: "Seed already exists",
            validationErrMessage: {
                exchangeName: "Exchange name is required",
                cyptocurrency: "Cyptocurrency is required",
                cyptocurrencyIndex: "cyptocurrency Index is required",                
                domainName: "Domain name is required",
                apiKey: "API key is required",
                from: "From address is required",
                to: "To address is required",
                amount: "Amount is required"
            }
        }
    };

})();