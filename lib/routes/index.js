((appllicationRoutes) => {
    'use strict';

    appllicationRoutes.init = (app) => {

        const HTTPStatus = require('http-status');
        const commonHelper = require('../common/common-helper-function');
        // const tokenAuthMiddleware = require('../middlewares/token-auth.middleware');
        // const rateLimiter = require('../middlewares/rate-limiter.middleware');
        // const emailValidator = require('../helpers/email-validator');
        // const blockChainService = require('../../xceltoken-service/lib/services/blockchain-service');

        // rateLimiter.init(app);

        // app.delete('/api/deletefile', tokenAuthMiddleware.authenticate, roleAuthMiddleware.authorizeAll, fileOperationHelper.deleteFile);
        // app.get('/api/download/file', tokenAuthMiddleware.authenticate, fileDownloadHelper.downPrivateUploads);


        // app.use(async(req, res, next) => {
        //     const content_type = req.get('Content-Type') || req.get('content-type')
        //     if (content_type && content_type.indexOf("multipart/form-data") >= 0) {
        //         req.multi_part_enctype_operation = true;
        //         next();
        //     } else {
        //         return emailValidator.validateEmailFormat(req, res, next);
        //     }

        // });


        // const authTokenRouter = require('../modules/auth-token/auth-token.route');
        // app.use('/api/configuration/authtoken', tokenAuthMiddleware.authenticate, authTokenRouter);

        const walletRouter = require('../modules/wallet/wallet.route');
        app.use('/api/wallet', walletRouter);

    };

})(module.exports);
