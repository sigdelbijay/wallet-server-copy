((providerHelper) => {
  'use strict';

  const Promise = require("bluebird");
  const join = Promise.join;
  const commonHelper = require('./common-helper-function');

  providerHelper.getPaginatedDataList = (Collection, queryOpts, pagerOpts, projectFields, sortOpts) => {
    return join(Collection
        .find(queryOpts, projectFields)
        .skip(pagerOpts.perPage * (pagerOpts.page-1))
        .limit(pagerOpts.perPage)
        .sort(sortOpts).toArray(), Collection.count(queryOpts),
      (dataList, count) => {
        return {
          dataList:dataList,
          totalItems:count,
          currentPage:pagerOpts.page
        };
      });
  };

  providerHelper.checkForDuplicateRecords = async (Collection, queryOpts, objSave) => {
    const count = await Collection.count(queryOpts);
    if(count > 0){
      return Promise.resolve({
        exists: true
      });
    }else{

      const dataRes = await Collection.save(objSave);
      return Promise.resolve({
        exists: false,
        dataRes: dataRes
      });
    }
  };

  providerHelper.checkForDuplicateRecordsForUpdate = async (Collection, queryOptsCount, queryOptsUpdate, objUpdate) => {
    const count = await Collection.count(queryOptsCount);
    if(count > 0){
      return Promise.resolve({
        exists: true
      });
    }else{
      const dataRes = await Collection.updateOne(queryOptsUpdate, {$set: objUpdate});
      return Promise.resolve({
        exists: false,
        dataRes: dataRes
      });
    }
  };


  providerHelper.getAllWithFieldsPaginationPopulation = (Model, queryOpts, pagerOpts, documentFields, populationPath, populationFields, populationQueryOpts) => {

    return join(Model.find(queryOpts, documentFields)
        .populate({
          path: populationPath,
          match: populationQueryOpts,
          select: populationFields
        })
        .skip(pagerOpts.perPage * (pagerOpts.page-1))
        .limit(pagerOpts.perPage)
        .sort({ added_on: -1 })
        .execAsync(), Model.count(queryOpts).execAsync(),
      (dataList, count) => {
        return {
          dataList:dataList,
          totalItems:count,
          currentPage:pagerOpts.page
        };
      });
  };

  providerHelper.getAllWithFieldsPaginationMultiPopulation = (Model, queryOpts, pagerOpts, documentFields, firstPopulation, secondPopulation, firstPopulationFields, populationQueryOpts, secondPopulationFields) => {

    return join(Model.find(queryOpts)
        .populate({
          path: firstPopulation,
          match: populationQueryOpts,
          select: firstPopulationFields
        })
        .populate({
          path: secondPopulation,
          select: secondPopulationFields
        })
        .select(documentFields)
        .skip(pagerOpts.perPage * (pagerOpts.page-1))
        .limit(pagerOpts.perPage)
        .sort({ added_on: -1 })
        .execAsync(), Model.count(queryOpts).execAsync(),
      (dataList, count) => {
        return {
          dataList:dataList,
          totalItems:count,
          currentPage:pagerOpts.page
        };
      });
  };

  providerHelper.getAllWithoutFieldsPaginationMultiPopulation = (Model, queryOpts, documentFields, firstPopulation, secondPopulation, firstPopulationFields, populationQueryOpts, secondPopulationFields) => {

    return Model.find(queryOpts)
      .populate({
        path: firstPopulation,
        match: populationQueryOpts,
        select: firstPopulationFields
      })
      .populate({
        path: secondPopulation,
        select: secondPopulationFields
      })
      .select(documentFields)
      .sort({ added_on: -1 })
      .execAsync();
  };

  providerHelper.getAllWithFieldsPopulation = (Model, queryOpts, documentFields, populationPath, populationFields, populationQueryOpts, sortOpts) => {
    return Model.find(queryOpts)
      .select(documentFields)
      .populate({
        path: populationPath,
        match: populationQueryOpts,
        select: populationFields,
        options: { sort: sortOpts }

      })
      .execAsync();
  };

  providerHelper.getByIdWithPopulation = (Model, queryOpts, populationPath, populationQueryOpts, sortOpts, populationFields, documentFields) => {
    return Model.findById(queryOpts)
      .select(documentFields)
      .populate({
        path: populationPath,
        match: populationQueryOpts,
        select: populationFields,
        options: { sort: sortOpts }
      })
      .execAsync();
  };

  providerHelper.getByIdWithMultiplePopulation = (Model, queryOpts, firstPopulation, secondPopulation, documentFields, firstPopulationField, secondPopulationField) => {
    return Model.findById(queryOpts)
      .select(documentFields)
      .populate({
        path: firstPopulation,
        select: firstPopulationField
      })
      .populate({
        path: secondPopulation,
        select: secondPopulationField
      })
      .execAsync();
  };

  providerHelper.getAllEmbeddedDocumentsWithDocumentFieldsPagination = (Model, queryOpts, pagerOpts, documentFields, sortParam, groupOpts, countProjectFields, unWindField) => {

    return join( Model.aggregate(
      {
        $unwind:unWindField
      },
      {
        $match:queryOpts
      },{
        $project:documentFields
      },
      {
        $sort:sortParam
      },{
        $skip:pagerOpts.perPage * (pagerOpts.page-1)
      },{
        $limit:pagerOpts.perPage
      },{
        $group:groupOpts
      }
      ).execAsync(), Model.aggregate(
      {
        $match:queryOpts
      },
      {
        $project:countProjectFields
      }
      ).execAsync(),
      (dataList, lstCount) => {
        let _totalItems = 0;
        if(lstCount.length > 0){
          _totalItems = lstCount[0].count;
        }
        return {
          dataList:dataList,
          totalItems:_totalItems,
          currentPage:pagerOpts.page
        };
      });
  };


  providerHelper.getEmbeddedDocumentsWithoutPagination = (Model, queryOpts, documentFields, unWindField, sortParam, groupOpts) => {

    return Model.aggregate(
      {
        $unwind:unWindField
      },
      {
        $match:queryOpts
      },
      {
        $sort:sortParam
      },{
        $project:documentFields
      },{
        $group:groupOpts
      }
    ).execAsync();

  };

})(module.exports);
