'use strict';


((commonHelper) => {
    'use strict';

    const HTTPStatus = require('http-status');
    const ObjectId = require('mongodb').ObjectID;
    const messageConfig = require('../configs/message.config');
    const disposableDomainList = require('../static-data/disposable-email-domains.json');
    const suspendedDomainList = require('../static-data/temporary-blocked-emails');
    // const geoip2 = require('geoip2');
    const Promise = require('bluebird');
    const randomBytes = require('randombytes');

    commonHelper.getLoggedInUser = (req) => {
        return (req.decoded && req.decoded.user && req.decoded.user.username) ? req.decoded.user.username : 'system';
    };

    commonHelper.getDisposableEmails = () => {
        return disposableDomainList;
    };

    commonHelper.getTemporarySuspendedEmails = () => {
        return suspendedDomainList;
    };

    commonHelper.checkDisposableEmail = (email) => {
        const mailDomain = email.replace(/.*@/, "");
        return !!(disposableDomainList.indexOf(mailDomain) > -1);
    };
    commonHelper.getLoggedInPersonName = (req) => {
        return (req.decoded && req.decoded.user && req.decoded.user.first_name) ? `${req.decoded.user.first_name} ${req.decoded.user.last_name}` : '';
    };

    commonHelper.getLoggedInUserId = (req) => {
        return (req.decoded && req.decoded.user && req.decoded.user._id) ? req.decoded.user._id : '';
    };

    commonHelper.getTextValFromObjectField = (_val) => {
        return (_val) ? _val : '';
    };

    commonHelper.checkAndGetParentUserRole = (req) => {
        return (req.decoded && req.decoded.user && req.decoded.user.parent_user_role) ? req.decoded.user.parent_user_role : null;
    };

    commonHelper.getLoggedInUserRole = (req) => {
        const parent_user_role = commonHelper.checkAndGetParentUserRole(req);
        return parent_user_role ? parent_user_role : (req.decoded && req.decoded.user && req.decoded.user.user_role) ? req.decoded.user.user_role : '';
    };

    commonHelper.getLoggedInUserEmail = (req) => {
        return (req.decoded && req.decoded.user && req.decoded.user.email) ? req.decoded.user.email : '';
    };

    commonHelper.sendJsonResponse = (res, data, message, status) => {
        res.status(status);
        const returnObj = data ? (status === HTTPStatus.NOT_FOUND ? {
            'status': status,
            'data': (data instanceof Array) ? [] : {},
            'message': message
        } : {
            'status': status,
            'data': data
        }) : {
            'status': status,
            'data': (data instanceof Array) ? [] : {},
        };
        res.json(returnObj);
    };

    commonHelper.sendDataManipulationMessage = (res, data, message, status) => {
        res.status(status);
        res.json({
            'status': status,
            'data': data,
            'message': message
        });
    };

    commonHelper.sendNormalResponse = (res, data, status) => {
        res.status(status);
        res.json({
            'status': status,
            'data': data,
        });
    };

    commonHelper.sendResponseData = (res, {status, message}) => {
        res.status(status);
        res.json({
            'status': status,
            'message': message
        });
    };


    commonHelper.getGeoLocationInfo = (ip_address) => {
        return new Promise((resolve, reject) => {
            geoip2.lookupSimple(ip_address, (err, result) => {
                if (err) {
                    resolve(null);
                } else {
                    resolve(result);
                }
            });
        });
    };

    commonHelper.collectFormFields = (req, sanitizedObj, specifiedFormFields, action) => {
        try {
            const specifiedFormFieldsArr = specifiedFormFields.split(' ');
            let dataObj = {};
            if (action === 'update') {
                dataObj = {
                    updated_by: commonHelper.getLoggedInUser(req),
                    updated_on: new Date()
                };
            } else {
                dataObj = {
                    _id: ObjectId(),
                    added_by: commonHelper.getLoggedInUser(req),
                    added_on: new Date(),
                    deleted: false
                };
            }
            for (let i = 0; i < specifiedFormFieldsArr.length; i++) {
                if (sanitizedObj[specifiedFormFieldsArr[i]] === 'true' || sanitizedObj[specifiedFormFieldsArr[i]] === 'false') {
                    dataObj[specifiedFormFieldsArr[i]] = sanitizedObj[specifiedFormFieldsArr[i]] === 'true' ? true : false;
                } else {
                    dataObj[specifiedFormFieldsArr[i]] = (sanitizedObj[specifiedFormFieldsArr[i]] !== null && sanitizedObj[specifiedFormFieldsArr[i]] !== undefined) ? sanitizedObj[specifiedFormFieldsArr[i]] : "";
                }
            }
            return dataObj;
        }
        catch (err) {
            // return  next(err);
        }
    };

    commonHelper.collectFormFieldsValidateAndConvertWithDataType = (req, sanitizeObj, model, action, next) => {
        try {
            let result = {};
            let isError = false;
            let error = [];
            for (let key in model) {
                if (model.hasOwnProperty(key)) {
                    let value = sanitizeObj[key]
                    for (let validate in model[key]) {
                        switch (model[key][validate].dt) {
                            case "Required":
                                if (value && value.length >= 0) {

                                } else {
                                    isError = true;
                                    error.push({
                                        location: 'body',
                                        param: key,
                                        msg: model[key][validate].errmsg,
                                        value: value
                                    })
                                }
                                break;
                            case "ObjectID":
                                try {
                                    value = ObjectId(value);
                                } catch (err) {
                                    isError = true;
                                    error.push({
                                        location: 'body',
                                        param: key,
                                        msg: model[key][validate].errmsg,
                                        value: value
                                    })
                                }
                                break;
                            case "Int":
                                try {
                                    value = parseInt(value) || 0;
                                } catch (err) {
                                    isError = true;
                                    error.push({
                                        location: 'body',
                                        param: key,
                                        msg: model[key][validate].errmsg,
                                        value: value
                                    })
                                }
                                break;
                            case "Bool":
                                try {
                                    value = JSON.parse(value);
                                } catch (err) {
                                    isError = true;
                                    error.push({
                                        location: 'body',
                                        param: key,
                                        msg: model[key][validate].errmsg,
                                        value: value
                                    })
                                }
                                break;
                            case "Decimal":
                                try {
                                    value = parseFloat(value);
                                } catch (err) {
                                    isError = true;
                                    error.push({
                                        location: 'body',
                                        param: key,
                                        msg: model[key][validate].errmsg,
                                        value: value
                                    })
                                }
                                break;
                            case "Date":
                                try {
                                    value = new Date(value);
                                } catch (err) {
                                    isError = true;
                                    error.push({
                                        location: 'body',
                                        param: key,
                                        msg: model[key][validate].errmsg,
                                        value: value
                                    })
                                }
                                break;
                            default :

                        }
                    }
                    result[key] = value;
                }
            }
            if (isError) {
                return {isError: true, errors: error};
            }
            // return {...result, ...sanitizeObj};

            if (action === 'update') {
                result = {
                    ...result, ...{
                        updated_by: commonHelper.getLoggedInUser(req),
                        updated_on: new Date()
                    }
                };
            } else {
                result = {
                    ...{
                        _id: ObjectId(),
                        added_by: commonHelper.getLoggedInUser(req),
                        added_on: new Date(),
                        deleted: false
                    },
                    ...result
                };
            }
            return result;
        } catch (err) {
            console.log(err);
        }
    };


    commonHelper.constructObject = (inputObj, collectFields) => {
        try {
            const specifiedFieldsArr = collectFields.split(' ');
            let dataObj = {};

            for (let i = 0; i < specifiedFieldsArr.length; i++) {
                dataObj[specifiedFieldsArr[i]] = inputObj[specifiedFieldsArr[i]];
            }
            return dataObj;
        }
        catch (err) {
            // return  next(err);
        }
    };

    commonHelper.sendResponseMessage = (res, dataRes, dataObj, messageResponse) => {
        if (dataRes && dataRes.result && dataRes.result.n > 0) {
            const returnObj = dataObj ? {
                status: HTTPStatus.OK,
                message: messageResponse,
                data: dataObj
            } : {
                status: HTTPStatus.OK,
                message: messageResponse
            };
            res.status(HTTPStatus.OK);
            return res.json(returnObj);
        } else {
            return commonHelper.sendResponseData(res, {
                status: HTTPStatus.NOT_MODIFIED,
                message: messageConfig.applicationMessage.dataNotModified
            });
        }
    };

    commonHelper.sendJsonResponseMessage = (res, dataRes, returnObj, messageResponse) => {
        if (dataRes && dataRes.result && dataRes.result.n > 0) {
            res.status(HTTPStatus.OK);
            res.json({
                'status': HTTPStatus.OK,
                'data': returnObj,
                'message': messageResponse
            });
        } else {
            return commonHelper.sendResponseData(res, {
                status: HTTPStatus.NOT_MODIFIED,
                message: messageConfig.applicationMessage.dataNotModified
            });
        }
    };

    commonHelper.generateRandomBytes=function (length) {
        return new Promise(function (resolve, reject) {
            randomBytes(length, function (err, resp){
                if (err){
                    reject(err);
                } else{
                    resolve(resp.toString('hex').substring(0,length));
                }
            });
        });
    };

})(module.exports);
